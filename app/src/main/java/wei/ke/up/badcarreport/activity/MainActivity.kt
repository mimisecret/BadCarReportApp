package wei.ke.up.badcarreport.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import wei.ke.up.badcarreport.R
import wei.ke.up.badcarreport.adapter.MainViewPagerAdapter
import wei.ke.up.badcarreport.databinding.ActivityMainBinding
import wei.ke.up.badcarreport.fragment.*
import wei.ke.up.badcarreport.view.BottomPageNavBar

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var currentPageIndex = BottomPageNavBar.Item.ITEM1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        PreferenceManager.getDefaultSharedPreferences(this)
            .let {
                val values = resources.getStringArray(R.array.dark_mode_values)
                it.getString(getString(R.string.flag_dark_mode), values[0])?.apply {
                    when (this) {
                        values[0] -> {
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                        }
                        values[1] -> {
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                        }
                        values[2] -> {
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                        }
                    }
                }
            }

        binding = ActivityMainBinding.inflate(layoutInflater).apply {
            bottomPageNavBar.apply {
                setOnItemClickListener { which, _ ->
                    when (which) {
                        BottomPageNavBar.Item.ITEM3 -> {
                        }
                        BottomPageNavBar.Item.ITEM_FAB -> {
                            mainViewPager.setCurrentItem(2, false)
                            currentPageIndex = BottomPageNavBar.Item.ITEM_FAB
                        }
                        else -> {
                            mainViewPager.setCurrentItem(which.ordinal, false)
                            currentPageIndex = which
                        }
                    }
                }

                val fragmentList = listOf(
                    HomeFragment(),
                    SearchLicensePlateFragment(),
                    RecognizeFragment(),
                    SyncDBFragment(),
                    SettingsFragment()
                )
                mainViewPager.adapter =
                    MainViewPagerAdapter(supportFragmentManager, lifecycle, fragmentList)
                mainViewPager.isUserInputEnabled = false

                savedInstanceState?.getInt(getString(R.string.flag_current_page_index))?.let {
                    callOnItemClick(BottomPageNavBar.Item.values()[it])
                }
            }
        }
        setContentView(binding.root)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(getString(R.string.flag_current_page_index), currentPageIndex.ordinal)
    }
}