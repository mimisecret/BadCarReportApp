package wei.ke.up.badcarreport.tflite

import android.graphics.Bitmap
import android.util.Log
import androidx.core.graphics.scale
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named
import wei.ke.up.badcarreport.module.Recognizer
import wei.ke.up.badcarreport.tflite.objr.ObjectRecognizer
import wei.ke.up.badcarreport.tflite.objr.Recognition
import wei.ke.up.badcarreport.util.crop
import wei.ke.up.badcarreport.util.scale

class LPRecognizer : KoinComponent {
    companion object {
        private val TAG = this::class.java.simpleName
    }

    private val objRecognizer by inject<ObjectRecognizer>(named(Recognizer.OBJECT))
    private val lpRecognizer by inject<ObjectRecognizer>(named(Recognizer.LICENSE_PLATE))
    private val lnRecognizer by inject<LNRecognizer>()

    fun recognize(bitmap: Bitmap): List<LPRecognition> {
        val lpResult = run {
            val imageToRec =
                bitmap.scale(lpRecognizer.inputSize.width, lpRecognizer.inputSize.height)
            val widthScale = bitmap.width.toFloat() / lpRecognizer.inputSize.width
            val heightScale = bitmap.height.toFloat() / lpRecognizer.inputSize.height
            lpRecognizer.recognize(imageToRec)
                .map {
                    val newLocation = it.location.scale(widthScale, heightScale)
                    Recognition(it.id, it.title, it.confidence, newLocation)
                }
                .filter {
                    it.location.width() > it.location.height()
                }
        }

        return mutableListOf<LPRecognition>().apply {
            for (recognition in lpResult) {
                try {
                    val croppedImage = bitmap.crop(recognition.location)
                    val imageToRec =
                        croppedImage.scale(
                            lnRecognizer.inputSize.width,
                            lnRecognizer.inputSize.height
                        )
                    val licenseNumber = lnRecognizer.recognize(imageToRec)
                        .trim()
                    if (licenseNumber.isNotEmpty()) {
                        add(LPRecognition(licenseNumber, recognition.location))
                    }
                } catch (ex: IllegalArgumentException) {
                    Log.e(TAG, "Recognition: $recognition", ex)
                }
            }
        }
    }
}