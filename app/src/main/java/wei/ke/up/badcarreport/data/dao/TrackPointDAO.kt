package wei.ke.up.badcarreport.data.dao

import androidx.room.*
import wei.ke.up.badcarreport.data.database.TypeConverter
import wei.ke.up.badcarreport.data.model.TrackPoint
import java.util.*

@Dao
@TypeConverters(TypeConverter::class)
interface TrackPointDAO {
    val size: Int
        @Query("SELECT COUNT(*) FROM track_point")
        get

    @Query("SELECT * FROM track_point")
    suspend fun getAll(): List<TrackPoint>

    @Query("SELECT * FROM track_point WHERE tid=:tid")
    suspend fun getAllById(tid: Int): List<TrackPoint>

    @Query("SELECT * FROM track_point WHERE licenseNumber=:licenseNumber")
    suspend fun getAllByLicenseNumber(licenseNumber: String): List<TrackPoint>

    @Query("SELECT * FROM track_point WHERE timestamp BETWEEN :from AND :to")
    suspend fun getAllInTimeRange(
        from: Date,
        to: Date
    ): List<TrackPoint>

    @Query("SELECT * FROM track_point WHERE licenseNumber=:licenseNumber AND timestamp BETWEEN :from AND :to")
    suspend fun getInTimeRange(
        licenseNumber: String,
        from: Date,
        to: Date
    ): List<TrackPoint>

    @Insert
    suspend fun add(vararg trackPoints: TrackPoint)

    @Update
    suspend fun update(vararg trackPoints: TrackPoint)

    @Delete
    suspend fun remove(vararg trackPoints: TrackPoint)

    @Query("DELETE FROM track_point WHERE licenseNumber IN (:licenseNumber)")
    suspend fun removeAllByLicenseNumber(vararg licenseNumber: String)

    @Query("DELETE FROM track_point")
    suspend fun removeAll()
}