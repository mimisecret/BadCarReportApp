package wei.ke.up.badcarreport.fragment

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import kotlinx.coroutines.*
import wei.ke.up.badcarreport.databinding.FragmentHomeBinding
import wei.ke.up.badcarreport.viewmodel.LicensePlateViewModel

class HomeFragment : Fragment(), CoroutineScope by MainScope() {
    private var nullableBinding: FragmentHomeBinding? = null
    private val binding get() = nullableBinding!!
    private val onlineLicensePlateViewModel: LicensePlateViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        nullableBinding = FragmentHomeBinding.inflate(inflater, container, false)
        onlineLicensePlateViewModel.lostLicensePlates.observe(viewLifecycleOwner) {
            if (it.isEmpty()) return@observe
            binding.apply {
                lostLPList.apply {
                    removeAllViews()
                    it.forEach {
                        val textView = TextView(requireContext()).apply {
                            layoutParams = ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            )
                            text = it.licenseNumber
                            textSize = 18f
                            textAlignment = View.TEXT_ALIGNMENT_CENTER
                            typeface = Typeface.DEFAULT_BOLD
                        }
                        addView(textView)
                    }
                }
            }
        }
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        onlineLicensePlateViewModel.getAllLostLicensePlate()
    }

    override fun onDestroy() {
        cancel()
        nullableBinding = null
        super.onDestroy()
    }
}