package wei.ke.up.badcarreport.tflite

import android.graphics.Rect

data class LPRecognition(
    val licenseNumber: String,
    val location: Rect
)
