package wei.ke.up.badcarreport.util

import android.content.Context
import android.graphics.Bitmap
import android.renderscript.*
import android.util.Size

object ImageUtil {
    /*
      Convert YUV-NV21 Data to Bitmap
      Code from: https://blog.csdn.net/bluegodisplay/article/details/53431798
       */
    private lateinit var renderScript: RenderScript
    private lateinit var yuvToRGB: ScriptIntrinsicYuvToRGB
    private lateinit var yuvType: Type
    private lateinit var rgbType: Type
    private lateinit var inAllocation: Allocation
    private lateinit var outAllocation: Allocation
    private var lastSize = Size(0, 0)
    private var lastDataLength = 0

    fun YuvToBitmap(context: Context, bytes: ByteArray, size: Size): Bitmap {
        if (!this::renderScript.isInitialized) {
            renderScript = RenderScript.create(context)
            yuvToRGB = ScriptIntrinsicYuvToRGB.create(renderScript, Element.U8_4(renderScript))
        }

        if (lastSize != size || lastDataLength != bytes.size) {
            yuvType =
                Type.Builder(renderScript, Element.U8(renderScript))
                    .setX(bytes.size)
                    .create()
            inAllocation =
                Allocation.createTyped(renderScript, yuvType, Allocation.USAGE_SCRIPT)

            rgbType =
                Type.Builder(renderScript, Element.RGBA_8888(renderScript))
                    .setX(size.width)
                    .setY(size.height)
                    .create()
            outAllocation =
                Allocation.createTyped(renderScript, rgbType, Allocation.USAGE_SCRIPT)
        }
        inAllocation.copyFrom(bytes)
        yuvToRGB.setInput(inAllocation)
        yuvToRGB.forEach(outAllocation)

        val bitmap =
            Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888)
        outAllocation.copyTo(bitmap)

        return bitmap
    }
}