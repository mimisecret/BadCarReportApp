package wei.ke.up.badcarreport.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.databinding.LayoutLicensePlateListElementBinding
import java.time.LocalDateTime

typealias OnItemClickListener = (data: LicensePlate) -> Unit

class LicensePlateListAdapter :
    RecyclerView.Adapter<LicensePlateListAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener = {}
    var data = listOf<LicensePlate>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = LayoutLicensePlateListElementBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(data[position])
        holder.setOnItemClickListener(onItemClickListener)
    }

    override fun getItemCount(): Int = data.size

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    class ViewHolder(private val binding: LayoutLicensePlateListElementBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var data: LicensePlate

        fun setData(licensePlate: LicensePlate) {
            data = licensePlate
            binding.apply {
                licenseNumber.text = licensePlate.licenseNumber
                licenseStatus.text = when (licensePlate.status) {
                    LicensePlate.Status.GOOD -> "良好"
                    LicensePlate.Status.LOST -> "可疑"
                    LicensePlate.Status.UNKNOWN -> "未知"
                }
                licenseStatus.setTextColor(
                    when (licensePlate.status) {
                        LicensePlate.Status.GOOD -> Color.GREEN
                        LicensePlate.Status.LOST -> Color.RED
                        LicensePlate.Status.UNKNOWN -> Color.LTGRAY
                    }
                )
                timestamp.text = LocalDateTime.now().toString()
            }
        }

        fun setOnItemClickListener(listener: OnItemClickListener) {
            binding.root.setOnClickListener {
                listener(data)
            }
        }
    }
}