package wei.ke.up.badcarreport.view.lpinfocard

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import com.google.android.gms.maps.GoogleMap
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.databinding.LayoutLpInfoCardBinding
import wei.ke.up.badcarreport.util.hideKeyboard
import wei.ke.up.badcarreport.util.toDp

@SuppressLint("ClickableViewAccessibility")
class LicensePlateInfoCard(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet) {

    companion object {
        private val TAG = this::class.java.simpleName
        private val ANIMATE_DURATION = 200L
    }

    private lateinit var googleMap: GoogleMap

    private val binding = LayoutLpInfoCardBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )

    init {
        binding.apply {
            mapView.apply {
                onCreate(null)
                onStart()
                getMapAsync {
                    googleMap = it
                }
            }

            cardScrollView.apply {
                val lp = layoutParams as CoordinatorLayout.LayoutParams
                lp.behavior = Behavior(context, attributeSet).apply {
                    setHideAction { hideCardContent() }
                }
                layoutParams = lp

                setOnTouchListener { _, event ->
                    when (event.action) {
                        MotionEvent.ACTION_UP -> {
                            if (cardContent.translationY >= 100.toDp(context)) {
                                hideCardContent()
                            } else {
                                resumeCardContentPosition()
                            }
                        }
                    }
                    false
                }
            }

            root.visibility = INVISIBLE
            post {
                hideCardContent()
            }
        }
    }

    private fun resumeCardContentPosition() {
        binding.apply {
            cardContent.animate()
                .translationY(0f)
                .setDuration(ANIMATE_DURATION)
                .start()
        }
    }

    fun hideCardContent() {
        binding.apply {
            cardContent.apply {
                animate().translationY(height.toFloat())
                    .setDuration(ANIMATE_DURATION)
                    .start()
            }
            root.apply {
                animate().alpha(0f)
                    .setDuration(ANIMATE_DURATION)
                    .withEndAction { visibility = GONE }
                    .start()
            }

            mapView.onPause()
        }
    }

    fun showCardContent(licensePlate: LicensePlate) {
        binding.apply {
            rootView.hideKeyboard()
            cardScrollView.scrollY = 0

            licenseNumber.text = licensePlate.licenseNumber
            licenseStatus.text = when (licensePlate.status) {
                LicensePlate.Status.GOOD -> "良好"
                LicensePlate.Status.LOST -> "失竊"
                LicensePlate.Status.UNKNOWN -> "未知"
            }
            licenseStatus.setTextColor(
                when (licensePlate.status) {
                    LicensePlate.Status.GOOD -> Color.GREEN
                    LicensePlate.Status.LOST -> Color.RED
                    LicensePlate.Status.UNKNOWN -> Color.LTGRAY
                }
            )
            common.text = licensePlate.common

            mapView.apply {
                onResume()
                googleMap.clear()
            }

            // Start showing animation
            root.apply {
                animate().alpha(1f)
                    .setDuration(ANIMATE_DURATION)
                    .withStartAction { visibility = VISIBLE }
                    .start()
            }
            cardContent.apply {
                animate().translationY(0f)
                    .setDuration(ANIMATE_DURATION)
                    .withStartAction { scrollY = 0 }
                    .start()
            }
        }
    }

    class Behavior(private val context: Context, private val attributeSet: AttributeSet? = null) :
        CoordinatorLayout.Behavior<View>(context, attributeSet) {
        private var hideAction = {}

        override fun onStartNestedScroll(
            coordinatorLayout: CoordinatorLayout,
            child: View,
            directTargetChild: View,
            target: View,
            axes: Int,
            type: Int
        ): Boolean {
            return axes == ViewCompat.SCROLL_AXIS_VERTICAL
        }

        override fun onNestedPreScroll(
            coordinatorLayout: CoordinatorLayout,
            child: View,
            target: View,
            dx: Int,
            dy: Int,
            consumed: IntArray,
            type: Int
        ) {
//            Log.d(TAG, "dx: $dx, dy: $dy")
            when {
                target.scrollY == 0 -> {
                    when {
                        coordinatorLayout.translationY - dy < 0 -> {
                            coordinatorLayout.translationY = 0f
                        }
                        else -> {
                            coordinatorLayout.translationY -= dy
                        }
                    }
                }
                coordinatorLayout.translationY != 0f -> {
                    target.scrollY = 0
                    when {
                        coordinatorLayout.translationY - dy < 0 -> {
                            coordinatorLayout.translationY = 0f
                        }
                        else -> {
                            coordinatorLayout.translationY -= dy
                        }
                    }
                }
            }
        }

        fun setHideAction(action: () -> Unit) {
            hideAction = action
        }
    }
}

class MapView(context: Context, attributeSet: AttributeSet? = null) :
    com.google.android.gms.maps.MapView(context, attributeSet) {

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_UP -> {
                parent.parent.requestDisallowInterceptTouchEvent(false)
            }
            MotionEvent.ACTION_DOWN -> {
                parent.parent.requestDisallowInterceptTouchEvent(true)
            }
        }
        return super.dispatchTouchEvent(ev)
    }
}
