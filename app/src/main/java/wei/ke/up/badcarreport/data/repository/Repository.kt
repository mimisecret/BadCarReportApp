package wei.ke.up.badcarreport.data.repository

import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.data.model.TrackPoint

interface Repository {
    suspend fun getAllLicensePlateOnDevice(): List<LicensePlate>
    suspend fun getAllLostLicensePlate(): List<LicensePlate>
    suspend fun addLicensePlate(licensePlate: LicensePlate, trackPoint: TrackPoint? = null)
    suspend fun getLicensePlate(licenseNumber: String): LicensePlate
    suspend fun removeLicensePlate(vararg licensePlates: LicensePlate)
    suspend fun getAllLicensePlates(): List<LicensePlate>
    suspend fun syncDb(remoteDocumentPath: String): Int
    suspend fun getAllTrackPoints(): List<TrackPoint>
    suspend fun removeAllLicensePlate()
}