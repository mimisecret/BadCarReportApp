package wei.ke.up.badcarreport.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.data.repository.Repository

class LicensePlateViewModel : ViewModel(), KoinComponent {
    private val mLostLicensePlates = MutableLiveData<List<LicensePlate>>()
    private val repository by inject<Repository>()

    val lostLicensePlates = mLostLicensePlates as LiveData<List<LicensePlate>>

    fun getAllLostLicensePlate() {
        viewModelScope.launch {
            val lostLicensePlates = repository.getAllLostLicensePlate()
            mLostLicensePlates.value = lostLicensePlates
        }
    }
}