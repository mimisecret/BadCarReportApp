package wei.ke.up.badcarreport.data.database

import androidx.room.TypeConverter
import wei.ke.up.badcarreport.data.model.LicensePlate
import java.util.*

class TypeConverter {
    @TypeConverter
    fun status2Int(status: LicensePlate.Status): Int = status.ordinal

    @TypeConverter
    fun int2Status(int: Int): LicensePlate.Status = LicensePlate.Status.values()[int]

    @TypeConverter
    fun date2UTCLong(date: Date): Long = date.time

    @TypeConverter
    fun utcLong2Date(long: Long): Date = Date(long)
}