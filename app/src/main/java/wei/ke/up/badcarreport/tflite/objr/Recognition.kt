package wei.ke.up.badcarreport.tflite.objr

import android.graphics.Rect

data class Recognition(
    val id: String,
    val title: String,
    val confidence: Float,
    val location: Rect
)
