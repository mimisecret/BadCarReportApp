package wei.ke.up.badcarreport.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import wei.ke.up.badcarreport.data.database.TypeConverter
import java.util.*

@Entity(tableName = "track_point")
@TypeConverters(TypeConverter::class)
data class TrackPoint(
    @PrimaryKey(autoGenerate = true)
    val tid: Int = 0,
    val licenseNumber: String = "",
    val timestamp: Date = Date(),
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TrackPoint

        if (licenseNumber != other.licenseNumber) return false
        if (timestamp != other.timestamp) return false
        if (latitude != other.latitude) return false
        if (longitude != other.longitude) return false

        return true
    }

    override fun hashCode(): Int {
        var result = licenseNumber.hashCode()
        result = 31 * result + timestamp.hashCode()
        result = 31 * result + latitude.hashCode()
        result = 31 * result + longitude.hashCode()
        return result
    }
}