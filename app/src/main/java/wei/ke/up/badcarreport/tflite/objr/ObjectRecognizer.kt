package wei.ke.up.badcarreport.tflite.objr

import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.util.Log
import android.util.Size
import org.tensorflow.lite.Interpreter
import wei.ke.up.badcarreport.util.size
import java.io.FileInputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.math.abs
import kotlin.streams.toList

class ObjectRecognizer private constructor(
    context: Context,
    modelFileName: String,
    labelmapFileName: String,
    private val isQuantized: Boolean
) {
    companion object {
        private val TAG = this::class.java.simpleName
        private const val IMAGE_MEAN = 127.5f
        private const val IMAGE_STD = 127.5f

        fun create(
            context: Context,
            modelFileName: String,
            labelmapFileName: String,
            isQuantized: Boolean
        ): ObjectRecognizer {
            return ObjectRecognizer(context, modelFileName, labelmapFileName, isQuantized)
        }
    }

    val inputSize: Size

    private val interpreter: Interpreter
    private val labelMap: List<String>
    private val imgData: ByteBuffer
    private val numBytesPerChannel = if (isQuantized) 1 else 4

    init {
        try {
            val model = loadModel(context.assets, modelFileName)
            val option = Interpreter.Options().apply {
                setNumThreads(4)
            }
            interpreter = Interpreter(model, option)
            interpreter.allocateTensors()
            inputSize = getModelInputDataSize()
            labelMap = loadLabelMap(context.assets, labelmapFileName)
            imgData =
                ByteBuffer.allocateDirect(1 * inputSize.height * inputSize.width * 3 * numBytesPerChannel)
            imgData.order(ByteOrder.nativeOrder())
        } catch (ex: Exception) {
            Log.e(TAG, "Load model error!", ex)
            throw ex
        }
    }

    private fun loadLabelMap(assetManager: AssetManager, fileName: String): List<String> {
        val inputStream = assetManager.open(fileName)
        val bufferedReader = inputStream.bufferedReader()
        val lines = bufferedReader.lines()
        return lines.toList()
    }

    private fun loadModel(assetManager: AssetManager, fileName: String): MappedByteBuffer {
        val fd = assetManager.openFd(fileName)
        val fis = FileInputStream(fd.fileDescriptor)
        val channel = fis.channel
        val startOffset = fd.startOffset
        val declaredLength = fd.declaredLength
        return channel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    private fun getModelInputDataSize() = interpreter.getInputTensor(0)
        .shape()
        .run {
            val height = this[1]
            val width = this[2]
            Size(width, height)
        }

    fun recognize(bitmap: Bitmap): List<Recognition> {
        if (bitmap.size != inputSize) {
            throw IllegalArgumentException("Input bitmap size ${bitmap.size} != inputSize $inputSize")
        }

        imgData.loadFromBitmap(bitmap, isQuantized)
        val inputs = arrayOf(imgData)

        val outputLocations = Array(1) { Array(10) { FloatArray(4) } }
        val outputClasses = Array(1) { FloatArray(10) }
        val outputScores = Array(1) { FloatArray(10) }
        val numOfRecognition = FloatArray(1)
        val outputMap = mapOf<Int, Any>(
            0 to outputLocations,
            1 to outputClasses,
            2 to outputScores,
            3 to numOfRecognition
        )

        try {
            interpreter.runForMultipleInputsOutputs(inputs, outputMap)
        } catch (ex: Exception) {
            Log.e(TAG, "Recognizing error!", ex)
            throw RuntimeException(ex)
        }

        val numOfOutputs = numOfRecognition[0].toInt()
        val outputRecognition = mutableListOf<Recognition>()
        for (i in 0 until numOfOutputs) {
            val location = Rect(
                abs((outputLocations[0][i][1] * inputSize.width).toInt()),
                abs((outputLocations[0][i][0] * inputSize.height).toInt()),
                abs((outputLocations[0][i][3] * inputSize.width).toInt()),
                abs((outputLocations[0][i][2] * inputSize.height).toInt())
            )
            val recognition = Recognition(
                i.toString(),
                labelMap[outputClasses[0][i].toInt()],
                outputScores[0][i],
                location
            )
            outputRecognition += recognition
        }

        return outputRecognition
    }

    private fun ByteBuffer.loadFromBitmap(bitmap: Bitmap, isQuantized: Boolean) {
        val pixels = IntArray(bitmap.width * bitmap.height)
        bitmap.getPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        this.rewind()
        pixels.forEach {
            val r = it shr 16 and 0xFF
            val g = it shr 8 and 0xFF
            val b = it and 0xFF

            if (isQuantized) {
                put(r.toByte())
                put(g.toByte())
                put(b.toByte())
            } else {
                putFloat((r - IMAGE_MEAN) / IMAGE_STD)
                putFloat((g - IMAGE_MEAN) / IMAGE_STD)
                putFloat((b - IMAGE_MEAN) / IMAGE_STD)
            }
        }
    }
}
