package wei.ke.up.badcarreport.util

import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Rect
import android.util.Size
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.image.TensorImage
import wei.ke.up.badcarreport.R
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.math.roundToInt

@Suppress("UNCHECKED_CAST")
fun <T : Number> T.toDp(context: Context): T {
    val dpi = context.resources.displayMetrics.density
    return when (this) {
        is Int -> (this * dpi).roundToInt() as T
        is Float -> (this * dpi) as T
        is Double -> (this * dpi) as T
        else -> throw NotImplementedError()
    }
}

fun String.showOnSnackBar(view: View) {
    Snackbar.make(view, this, Snackbar.LENGTH_SHORT)
        .show()
}

fun String.showOnDialog(context: Context, title: String) {
    AlertDialog.Builder(context)
        .setMessage(this)
        .setTitle(title)
        .setPositiveButton(context.getString(R.string.confirm)) { dialogInterface: DialogInterface, _: Int ->
            dialogInterface.dismiss()
        }
        .show()
}

fun View.hideKeyboard() {
    context.getSystemService(InputMethodManager::class.java)
        .hideSoftInputFromWindow(windowToken, 0)
}

val Bitmap.size: Size
    get() = Size(width, height)

fun Bitmap.toTensorImage(dataType: DataType) =
    TensorImage(dataType).also {
        it.load(this)
    }

fun Bitmap.crop(rect: Rect): Bitmap = Bitmap.createBitmap(
    this,
    rect.left,
    rect.top,
    rect.width(),
    rect.height()
)

fun Rect.scale(widthRatio: Float, heightRatio: Float): Rect =
    Rect(
        (this.left * widthRatio).toInt(),
        (this.top * heightRatio).toInt(),
        (this.right * widthRatio).toInt(),
        (this.bottom * heightRatio).toInt()
    )

suspend fun <T> Task<T>.await(): Result<T> = suspendCoroutine { continuation ->
    this@await.addOnCompleteListener {
        if (it.isSuccessful) {
            continuation.resume(Result.success(it.result))
        } else {
            continuation.resume(Result.failure(it.exception!!))
        }
    }
}