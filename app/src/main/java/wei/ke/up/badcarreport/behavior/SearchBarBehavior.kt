package wei.ke.up.badcarreport.behavior

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import androidx.core.view.marginTop
import wei.ke.up.badcarreport.R

class SearchBarBehavior(context: Context, attributeSet: AttributeSet) :
    CoordinatorLayout.Behavior<View>(context, attributeSet) {
    companion object {
        private val TAG = this::class.simpleName
    }

    override fun onStartNestedScroll(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        directTargetChild: View,
        target: View,
        axes: Int,
        type: Int
    ): Boolean {
        return target.id == R.id.scrollableContainer && axes == ViewCompat.SCROLL_AXIS_VERTICAL
    }

    override fun onNestedPreScroll(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        target: View,
        dx: Int,
        dy: Int,
        consumed: IntArray,
        type: Int
    ) {
//        Log.d(TAG, "dy: $dy")
//        Log.d(TAG, "Scroll Y: ${target.scrollY}")
//        Log.d(TAG, "Child top: ${child.top}")
        super.onNestedPreScroll(coordinatorLayout, child, target, dx, dy, consumed, type)
        when {
            child.bottom < 0 -> { // Keep hiding
                child.offsetTopAndBottom(-child.bottom)
            }
            dy > 0 -> { // Hide child
                child.offsetTopAndBottom(-dy)
            }
            child.top - dy > child.marginTop -> { // Freeze on top
                child.offsetTopAndBottom(-child.top + child.marginTop)
            }
            dy < 0 -> { // Show child
                child.offsetTopAndBottom(-dy)
            }
        }
    }
}