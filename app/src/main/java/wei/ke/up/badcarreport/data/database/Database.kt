package wei.ke.up.badcarreport.data.database

import wei.ke.up.badcarreport.data.dao.LicensePlateDAO
import wei.ke.up.badcarreport.data.dao.TrackPointDAO

interface Database {
    val licensePlateDAO: LicensePlateDAO
    val trackPointDAO: TrackPointDAO
}
