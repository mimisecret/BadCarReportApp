package wei.ke.up.badcarreport.module

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.koin.core.qualifier.named
import org.koin.dsl.module
import wei.ke.up.badcarreport.data.database.Database
import wei.ke.up.badcarreport.data.database.LocalDatabase
import wei.ke.up.badcarreport.data.repository.Repository
import wei.ke.up.badcarreport.data.repository.RepositoryImpl
import wei.ke.up.badcarreport.odmoigov.GovLostLicensePlateApi
import wei.ke.up.badcarreport.tflite.LNRecognizer
import wei.ke.up.badcarreport.tflite.LPRecognizer
import wei.ke.up.badcarreport.tflite.objr.ObjectRecognizer

enum class DataSource {
    LOCAL,
    REMOTE
}

val myModule = module {
    single<Database>(named(DataSource.LOCAL)) { LocalDatabase.getInstance(get()) }
    single<Repository> { RepositoryImpl() }
    single { GovLostLicensePlateApi }
    single { Firebase.firestore }
}

enum class Recognizer {
    OBJECT,
    LICENSE_PLATE
}

val recognizerModule = module {
    single { LNRecognizer(get()) }
    single(named(Recognizer.OBJECT)) {
        ObjectRecognizer.create(
            get(),
            "ObjectModel.tflite",
            "ObjectLabelmap.txt",
            true
        )
    }
    single(named(Recognizer.LICENSE_PLATE)) {
        ObjectRecognizer.create(
            get(),
            "LicensePlateModel.tflite",
            "LicensePlateLabelmap.txt",
            false
        )
    }
    single { LPRecognizer() }
}