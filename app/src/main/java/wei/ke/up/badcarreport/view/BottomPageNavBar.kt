package wei.ke.up.badcarreport.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.children
import com.google.android.material.floatingactionbutton.FloatingActionButton
import wei.ke.up.badcarreport.R
import wei.ke.up.badcarreport.databinding.LayoutBottomPageNavBarBinding
import wei.ke.up.badcarreport.util.toDp

typealias OnItemClickListener = BottomPageNavBar.(which: BottomPageNavBar.Item, view: View) -> Unit

class BottomPageNavBar(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet) {

    private val TAG = this::class.java.simpleName
    private var binding: LayoutBottomPageNavBarBinding =
        LayoutBottomPageNavBarBinding.inflate(LayoutInflater.from(context))
    private var onItemClickListener: OnItemClickListener = { _, _ -> }
    private var itemList: List<View>
    private var isFABShowing = true
        set(value) {
            binding.apply {
                when (value) {
                    true -> {
                        itemFAB.apply {
                            animate().alphaBy(0f)
                                .alpha(1f)
                                .translationYBy((-20f).toDp(context))
                                .translationY(0f)
                                .scaleXBy(0.5f)
                                .scaleX(1f)
                                .scaleYBy(0.5f)
                                .scaleY(1f)
                                .setDuration(150)
                                .start()
                        }
                        item3.apply {
                            animate().alphaBy(1f)
                                .alpha(0f)
                                .setDuration(150)
                                .withEndAction {
                                    visibility = INVISIBLE
                                }
                                .start()
                        }
                    }
                    false -> {
                        itemFAB.apply {
                            animate().alphaBy(1f)
                                .alpha(0f)
                                .translationYBy(0f)
                                .translationY(20f.toDp(context))
                                .scaleXBy(1f)
                                .scaleX(0.5f)
                                .scaleYBy(1f)
                                .scaleY(0.5f)
                                .setDuration(150)
                                .start()
                        }
                        item3.apply {
                            animate().alphaBy(0f)
                                .alpha(1f)
                                .setDuration(150)
                                .withStartAction {
                                    visibility = VISIBLE
                                }
                                .start()
                        }
                    }
                }
            }
            field = value
        }

    enum class Item {
        ITEM1,
        ITEM2,
        ITEM3,
        ITEM4,
        ITEM5,
        ITEM_FAB
    }

    init {
        val attrs = context.theme.obtainStyledAttributes(
            attributeSet,
            R.styleable.BottomPageNavBar,
            0,
            0
        )

        binding.apply {
            itemList = listOf(item1, item2, item3, item4, item5, itemFAB)

            val itemTitleList = listOf(
                attrs.getString(R.styleable.BottomPageNavBar_item1Title) ?: "Item1",
                attrs.getString(R.styleable.BottomPageNavBar_item2Title) ?: "Item2",
                attrs.getString(R.styleable.BottomPageNavBar_item3Title) ?: "Item3",
                attrs.getString(R.styleable.BottomPageNavBar_item4Title) ?: "Item4",
                attrs.getString(R.styleable.BottomPageNavBar_item5Title) ?: "Item5",
                "ItemFAB"
            )

            val itemIconList = listOf(
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item1Icon,
                    android.R.drawable.checkbox_off_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item2Icon,
                    android.R.drawable.checkbox_off_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item3Icon,
                    android.R.drawable.checkbox_off_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item4Icon,
                    android.R.drawable.checkbox_off_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item5Icon,
                    android.R.drawable.checkbox_off_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_itemFABIcon,
                    android.R.drawable.checkbox_on_background
                )
            )

            val itemSelectedIconList = listOf(
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item1SelectedIcon,
                    android.R.drawable.checkbox_on_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item2SelectedIcon,
                    android.R.drawable.checkbox_on_background
                ),
                0, // Item3
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item4SelectedIcon,
                    android.R.drawable.checkbox_on_background
                ),
                attrs.getResourceId(
                    R.styleable.BottomPageNavBar_item5SelectedIcon,
                    android.R.drawable.checkbox_on_background
                ),
                0 // FAB
            )

            val onClickListener = OnClickListener {
                itemList.forEachIndexed { index, item ->
                    when (item.tag) {
                        Item.ITEM3, Item.ITEM_FAB -> {
                        }
                        it.tag -> {
                            (item as LinearLayout).children.forEach {
                                when (it) {
                                    is ImageView -> {
                                        it.setImageResource(itemSelectedIconList[index])
                                    }
                                }
                            }
                        }
                        else -> {
                            (item as LinearLayout).children.forEach {
                                when (it) {
                                    is ImageView -> {
                                        it.setImageResource(itemIconList[index])
                                    }
                                }
                            }
                        }
                    }
                }

                when (it.tag) {
                    Item.ITEM3 -> {
                    }
                    Item.ITEM_FAB -> {
                        isFABShowing = false
                    }
                    else -> {
                        isFABShowing = true
                    }
                }

                onItemClickListener(
                    it.tag as Item,
                    it
                )
            }

            itemList.forEachIndexed { index, item ->
                item.tag = Item.values()[index]
                item.setOnClickListener(onClickListener)
                when (item) {
                    is FloatingActionButton -> {
                        item.setImageResource(itemIconList[index])
                    }
                    is LinearLayout -> {
                        item.children.forEach {
                            when (it) {
                                is ImageView -> {
                                    it.setImageResource(itemIconList[index])
                                }
                                is TextView -> {
                                    it.text = itemTitleList[index]
                                }
                            }
                        }
                    }
                }
            }

            item1.callOnClick()
        }

        addView(
            binding.root,
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        )

        attrs.recycle()
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    fun callOnItemClick(item: Item) {
        itemList[item.ordinal].callOnClick()
    }
}