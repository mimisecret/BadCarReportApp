package wei.ke.up.badcarreport.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import wei.ke.up.badcarreport.data.database.TypeConverter

@Entity(tableName = "license_plate")
@TypeConverters(TypeConverter::class)
data class LicensePlate(
    @PrimaryKey
    val licenseNumber: String = "",
    val status: Status = Status.UNKNOWN,
    val common: String = ""
) {
    enum class Status {
        GOOD,
        LOST,
        UNKNOWN
    }


}