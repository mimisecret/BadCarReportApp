package wei.ke.up.badcarreport.application

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import wei.ke.up.badcarreport.module.myModule
import wei.ke.up.badcarreport.module.recognizerModule

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MyApplication)
            androidLogger()
            modules(myModule, recognizerModule)
        }
    }
}