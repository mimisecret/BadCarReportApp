package wei.ke.up.badcarreport.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.util.toDp

class LPRecognitionView(context: Context, attributeSet: AttributeSet?) :
    View(context, attributeSet) {
    private var recognitions = mutableListOf<Pair<LicensePlate, Rect>>()
    private val paint = Paint().apply {
        color = Color.RED
        strokeWidth = 5f
        style = Paint.Style.STROKE
    }
    private val textPaint = Paint().apply {
        color = Color.BLACK
        textSize = 20f.toDp(context)
    }
    private val textBgPaint = Paint()

    override fun onDraw(canvas: Canvas) {
        recognitions.forEach { (licensePlate, rect) ->
            canvas.drawRect(rect, paint)
            canvas.drawRect(
                rect.left.toFloat(),
                rect.top - textPaint.textSize,
                rect.left + textPaint.measureText(licensePlate.licenseNumber),
                rect.top.toFloat(),
                textBgPaint.apply {
                    color = when (licensePlate.status) {
                        LicensePlate.Status.LOST -> Color.argb(200, 255, 0, 0)
                        else -> Color.argb(200, 255, 255, 255)
                    }
                }
            )
            canvas.drawText(
                licensePlate.licenseNumber,
                rect.left.toFloat(),
                rect.top.toFloat(),
                textPaint
            )
        }
    }

    fun drawLicensePlate(licensePlate: LicensePlate, rect: Rect) {
        recognitions.add(licensePlate to rect)
        invalidate()
    }

    fun clear() {
        recognitions.clear()
    }
}