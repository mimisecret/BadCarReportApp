package wei.ke.up.badcarreport.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Matrix
import android.location.Location
import android.media.Image
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.google.android.gms.location.*
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.otaliastudios.cameraview.frame.Frame
import kotlinx.coroutines.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import wei.ke.up.badcarreport.R
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.data.model.TrackPoint
import wei.ke.up.badcarreport.data.repository.Repository
import wei.ke.up.badcarreport.databinding.FragmentRecognizeBinding
import wei.ke.up.badcarreport.tflite.LPRecognition
import wei.ke.up.badcarreport.tflite.LPRecognizer
import wei.ke.up.badcarreport.util.ImageUtil
import wei.ke.up.badcarreport.util.Permission
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class RecognizeFragment : Fragment(), CoroutineScope by MainScope() {
    private val TAG = this::class.java.simpleName
    private var nullableBinding: FragmentRecognizeBinding? = null
    private val binding get() = nullableBinding!!

    private val lpRecognizer by inject<LPRecognizer>()
    private val lprResult = MutableLiveData<List<LPRecognition>>()
    private val recognizingFPS = MutableLiveData(0)
    private val repository by inject<Repository>()
    private val maxRecognitionCount: Int
        get() {
            return PreferenceManager.getDefaultSharedPreferences(requireContext())
                .getInt(
                    getString(R.string.flag_recognition_count),
                    resources.getInteger(R.integer.default_max_recognition_count)
                )
        }

    private val permissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private val currentLocation = MutableLiveData<LocationResult>()
    private val fusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(requireContext())
    }
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            currentLocation.value = locationResult
        }
    }

    private val mediaStoreDir =
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            .resolve(get<Context>().getString(R.string.app_name))
            .also {
                if (!it.exists()) {
                    it.mkdir()
                }
            }
    private val videoStoreDir = mediaStoreDir.resolve("Video")
        .also {
            if (!it.exists()) {
                it.mkdir()
            }
        }
    private val photoStoreDir = mediaStoreDir.resolve("Photo")
        .also {
            if (!it.exists()) {
                it.mkdir()
            }
        }

    private val dateFormat = SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault())

    private val cameraListener = object : CameraListener() {
        override fun onPictureTaken(result: PictureResult) {
            super.onPictureTaken(result)
            photoStoreDir.let {
                val fileName = dateFormat.format(Date())
                val picturePath = it.resolve("$fileName.jpg")
                result.toFile(picturePath) {}
            }
        }

        override fun onVideoRecordingStart() {
            super.onVideoRecordingStart()
            isTakingVideo.value = true
        }

        override fun onVideoRecordingEnd() {
            super.onVideoRecordingEnd()
            isTakingVideo.value = false
        }
    }
    private var isTakingVideo = MutableLiveData(false)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        nullableBinding = FragmentRecognizeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            bindLocationView()
            bindCameraView()

            recognizingFPS.observe(viewLifecycleOwner) {
                fpsView.text = "FPS: $it"
            }
        }
    }

    private fun FragmentRecognizeBinding.bindLocationView() {
        lprResult.observe(viewLifecycleOwner) {
            lpRecognitionView.clear()
            it.forEach { recognition ->
                launch(Dispatchers.IO) {
                    currentLocation.value?.let { locationResult ->
                        saveLicenseNumberWithLocation(
                            recognition.licenseNumber,
                            locationResult.lastLocation
                        )
                    }
                }
                launch(Dispatchers.IO) {
                    val licensePlate = repository.getLicensePlate(recognition.licenseNumber)
                    withContext(Dispatchers.Main) {
                        lpRecognitionView.drawLicensePlate(licensePlate, recognition.location)
                    }
                }
            }
        }

        currentLocation.observe(viewLifecycleOwner) {
            val lastLocation = it.lastLocation
            gpsStatus.text = "%f, %f\n%f".format(
                lastLocation.latitude,
                lastLocation.longitude,
                lastLocation.accuracy
            )
        }
    }

    private fun FragmentRecognizeBinding.bindCameraView() {
        cameraView.apply {
            addFrameProcessor {
                processFrame(it)
            }
            addCameraListener(cameraListener)
        }

        takePhotoBtn.setOnClickListener {
            cameraView.takePictureSnapshot()
        }

        takeVideoBtn.setOnClickListener {
            if (isTakingVideo.value!!) {
                cameraView.stopVideo()
            } else {
                val fileName = dateFormat.format(Date())
                cameraView.takeVideoSnapshot(videoStoreDir.resolve("$fileName.mp4"))
            }
        }

        val takeVideoColor =
            resources.getColor(R.color.takeVideoBtn, requireContext().theme)
        val stopTakingVideoColor =
            resources.getColor(R.color.stopTakingVideoBtn, requireContext().theme)
        isTakingVideo.observe(viewLifecycleOwner) {
            if (it) {
                takeVideoBtn.backgroundTintList = ColorStateList.valueOf(stopTakingVideoColor)
                takeVideoBtn.text = "Stop"
                takePhotoBtn.isEnabled = false
            } else {
                takeVideoBtn.backgroundTintList = ColorStateList.valueOf(takeVideoColor)
                takeVideoBtn.text = "Take a video"
                takePhotoBtn.isEnabled = true
            }
        }
    }

    private fun processFrame(frame: Frame) {
        val startTimeMillis = System.currentTimeMillis()
        val bitmap = frame.toBitmap()
        val licenseNumbers = lpRecognizer.recognize(bitmap)
            .take(maxRecognitionCount)
        val endTimeMillis = System.currentTimeMillis()
        recognizingFPS.postValue((1000.0 / (endTimeMillis - startTimeMillis)).roundToInt())
        lprResult.postValue(licenseNumbers)
    }

    private fun Frame.toBitmap() = when (this.dataClass) {
        ByteArray::class.java -> {
            val bytes = this.getData<ByteArray>()
            val bitmap = ImageUtil.YuvToBitmap(
                requireContext(),
                bytes,
                this.size.run { Size(width, height) }
            )
            val matrix = Matrix().apply {
                preRotate(this@toBitmap.rotationToUser.toFloat())
            }
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false)
        }
        Image::class.java -> {
            TODO("Handle frame data with Image class")
        }
        else -> {
            Log.wtf(TAG, "Frame data class: ${this.dataClass.simpleName}")
            throw RuntimeException("Unknown data class of frame data!")
        }
    }

    private suspend fun saveLicenseNumberWithLocation(licenseNumber: String, location: Location) {
        val trackPoint = TrackPoint(
            licenseNumber = licenseNumber,
            latitude = location.latitude,
            longitude = location.longitude
        )
        val licensePlate = LicensePlate(licenseNumber)
        repository.addLicensePlate(licensePlate, trackPoint)
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        Permission.request(this, *permissions) {
            binding.apply {
                cameraView.setLifecycleOwner(this@RecognizeFragment)
            }
            fusedLocationProviderClient.requestLocationUpdates(
                LocationRequest.create()
                    .setInterval(1000),
                locationCallback,
                Looper.getMainLooper()
            )
        }
    }

    override fun onPause() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        super.onPause()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Permission.onRequestPermissionsResult(
            requireContext(),
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun onDestroy() {
        cancel()
        nullableBinding = null
        super.onDestroy()
    }
}
