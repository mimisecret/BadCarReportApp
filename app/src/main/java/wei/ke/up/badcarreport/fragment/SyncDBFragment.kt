package wei.ke.up.badcarreport.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import wei.ke.up.badcarreport.data.repository.Repository
import wei.ke.up.badcarreport.databinding.FragmentSyncDbBinding
import wei.ke.up.badcarreport.util.showOnSnackBar

class SyncDBFragment : Fragment(), CoroutineScope by MainScope() {
    private var nullableBinding: FragmentSyncDbBinding? = null
    private val binding get() = nullableBinding!!

    private val repository by inject<Repository>()
    private val isSyncing = MutableLiveData(false)
    private var syncingJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        nullableBinding = FragmentSyncDbBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        isSyncing.observe(viewLifecycleOwner) {
            when (it) {
                true -> {
                    binding.apply {
                        btnStartSync.visibility = View.INVISIBLE
                        btnCancelSync.visibility = View.VISIBLE
                        progressBar.visibility = View.VISIBLE
                    }
                }
                false -> {
                    binding.apply {
                        btnStartSync.visibility = View.VISIBLE
                        btnCancelSync.visibility = View.INVISIBLE
                        progressBar.visibility = View.INVISIBLE
                    }
                }
            }
        }

        binding.apply {
            btnStartSync.setOnClickListener {
                isSyncing.value = true
                syncingJob = launch {
                    val numOfChanged = repository.syncDb("test/android")
                    "已更新 $numOfChanged 筆資料".showOnSnackBar(root)
                    isSyncing.value = false
                }
            }

            btnCancelSync.setOnClickListener {
                syncingJob?.cancel()
                isSyncing.value = false
            }
        }
    }

    override fun onDestroy() {
        cancel()
        nullableBinding = null
        super.onDestroy()
    }
}