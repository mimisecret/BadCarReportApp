package wei.ke.up.badcarreport.util

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.fragment.app.Fragment

object Permission {
    private val REQUEST_PERMISSION = 1
    private var allGranted: () -> Unit = {}
    private var dismissRequest = false

    private fun checkPermissions(context: Context, vararg permissions: String): Boolean {
        return permissions.all {
            context.checkSelfPermission(it) == PackageManager.PERMISSION_GRANTED
        }
    }

    fun request(fragment: Fragment, vararg permissions: String, allGranted: () -> Unit) {
        if (dismissRequest) {
            dismissRequest = false
            return
        }
        this.allGranted = allGranted
        when (checkPermissions(fragment.requireContext(), *permissions)) {
            true -> {
                allGranted()
            }
            false -> {
                fragment.requestPermissions(permissions, REQUEST_PERMISSION)
            }
        }
    }

    fun request(activity: Activity, vararg permissions: String, allGranted: () -> Unit) {
        if (dismissRequest) {
            dismissRequest = false
            return
        }
        this.allGranted = allGranted
        when (checkPermissions(activity, *permissions)) {
            true -> {
                allGranted()
            }
            false -> {
                activity.requestPermissions(permissions, REQUEST_PERMISSION)
            }
        }
    }

    fun onRequestPermissionsResult(
        context: Context,
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSION -> {
                dismissRequest = true
                when {
                    grantResults.all { it == PackageManager.PERMISSION_GRANTED } -> {
                        allGranted()
                    }
                    else -> {
                        val deniedPermissions = mutableListOf<String>()
                        grantResults.forEachIndexed { index, i ->
                            when (i) {
                                PackageManager.PERMISSION_DENIED -> {
                                    deniedPermissions.add(permissions[index])
                                }
                            }
                        }
                        deniedPermissions.joinToString("\n")
                            .showOnDialog(context, "需要權限！")
                    }
                }
            }
        }
    }
}