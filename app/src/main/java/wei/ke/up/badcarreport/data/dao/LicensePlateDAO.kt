package wei.ke.up.badcarreport.data.dao

import androidx.room.*
import wei.ke.up.badcarreport.data.database.TypeConverter
import wei.ke.up.badcarreport.data.model.LicensePlate

@Dao
@TypeConverters(TypeConverter::class)
interface LicensePlateDAO {
    val size: Int
        @Query("SELECT COUNT(*) FROM license_plate")
        get

    @Query("SELECT * FROM license_plate")
    suspend fun getAll(): List<LicensePlate>

    @Query("SELECT * FROM license_plate WHERE licenseNumber=:licenseNumber")
    suspend fun getByLicenseNumber(licenseNumber: String): LicensePlate?

    @Query("SELECT * FROM license_plate WHERE status=:status")
    suspend fun getAllByStatus(status: LicensePlate.Status): List<LicensePlate>

    @Insert
    suspend fun add(vararg licensePlates: LicensePlate)

    @Update
    suspend fun update(vararg licensePlates: LicensePlate): Int

    @Delete
    suspend fun remove(vararg licensePlates: LicensePlate)

    @Query("DELETE FROM license_plate")
    suspend fun removeAll()
}