package wei.ke.up.badcarreport.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import wei.ke.up.badcarreport.R
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.data.model.TrackPoint

@Database(version = 1, entities = [LicensePlate::class, TrackPoint::class], exportSchema = false)
abstract class LocalDatabase : RoomDatabase(), wei.ke.up.badcarreport.data.database.Database {
    companion object {
        private var database: LocalDatabase? = null
        fun getInstance(context: Context): LocalDatabase {
            if (database == null || !database!!.isOpen) {
                database = if (context.resources.getBoolean(R.bool.database_in_memory_mode))
                    Room.inMemoryDatabaseBuilder(
                        context,
                        LocalDatabase::class.java
                    ).build()
                else
                    Room.databaseBuilder(
                        context,
                        LocalDatabase::class.java,
                        context.getString(R.string.local_database_name)
                    ).build()
            }
            return database!!
        }
    }
}