package wei.ke.up.badcarreport.data.repository

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named
import wei.ke.up.badcarreport.data.database.Database
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.data.model.TrackPoint
import wei.ke.up.badcarreport.module.DataSource
import wei.ke.up.badcarreport.odmoigov.GovLostLicensePlateApi
import wei.ke.up.badcarreport.util.await

class RepositoryImpl : Repository, KoinComponent {
    companion object {
        private val TAG = RepositoryImpl::class.java.simpleName
    }

    private val localDb by inject<Database>(named(DataSource.LOCAL))
    private val firestore by inject<FirebaseFirestore>()
    private val govLostLicensePlateApi by inject<GovLostLicensePlateApi>()

    override suspend fun getAllLicensePlateOnDevice(): List<LicensePlate> {
        return localDb.licensePlateDAO.getAll()
    }

    override suspend fun getAllLostLicensePlate(): List<LicensePlate> {
        return localDb.licensePlateDAO.getAllByStatus(LicensePlate.Status.LOST)
    }

    override suspend fun addLicensePlate(
        licensePlate: LicensePlate,
        trackPoint: TrackPoint?
    ) {
        val lp = localDb.licensePlateDAO.getByLicenseNumber(licensePlate.licenseNumber)
        if (lp == null) {
            localDb.licensePlateDAO.add(licensePlate)
        } else {
            localDb.licensePlateDAO.update(lp)
        }

        trackPoint?.let {
            localDb.trackPointDAO.add(it)
        }
    }

    override suspend fun getLicensePlate(licenseNumber: String): LicensePlate {
        var licensePlate = localDb.licensePlateDAO.getByLicenseNumber(licenseNumber)
        if (licensePlate == null) {
            licensePlate = govLostLicensePlateApi.getLicensePlate(licenseNumber)
            addLicensePlate(licensePlate)
        }
        return licensePlate
    }

    override suspend fun removeLicensePlate(vararg licensePlates: LicensePlate) {
        localDb.licensePlateDAO.remove(*licensePlates)
        localDb.trackPointDAO
            .removeAllByLicenseNumber(*licensePlates.map { it.licenseNumber }.toTypedArray())
    }

    override suspend fun getAllLicensePlates(): List<LicensePlate> {
        return localDb.licensePlateDAO.getAll()
    }

    override suspend fun getAllTrackPoints(): List<TrackPoint> {
        return localDb.trackPointDAO.getAll()
    }

    override suspend fun removeAllLicensePlate() {
        localDb.licensePlateDAO.removeAll()
        localDb.trackPointDAO.removeAll()
    }

    override suspend fun syncDb(remoteDocumentPath: String): Int = withContext(IO) {
        val syncResult = syncLicensePlate(remoteDocumentPath)
        val uploadResult = uploadAndRemoveAllTrackPoints(remoteDocumentPath)

        return@withContext syncResult + uploadResult
    }

    private suspend fun syncLicensePlate(rootDoc: String): Int {
        val licensePlateCollection = firestore.collection("$rootDoc/licensePlate")
        val licensePlateDocuments = licensePlateCollection.get()
            .await()
            .getOrThrow()
            .documents
            .map {
                it.toObject<LicensePlate>()!!
            }
        val licensePlateDAO = localDb.licensePlateDAO
        var changedDataCount = 0

        licensePlateDocuments.forEach {
            val licensePlate = licensePlateDAO.getByLicenseNumber(it.licenseNumber)

            if (licensePlate == null) {
                licensePlateDAO.add(it)
                ++changedDataCount
            } else {
                changedDataCount += licensePlateDAO.update(it)
            }
        }

        val diffLicensePlates = licensePlateDAO.getAll()
            .filterNot { licensePlateDocuments.contains(it) }
        val result = firestore.runTransaction { transaction ->
            diffLicensePlates.forEach { licensePlate ->
                val docRef = licensePlateCollection.document(licensePlate.licenseNumber)
                transaction.set(docRef, licensePlate)
            }
        }.await()

        return if (result.isSuccess) {
            Log.d(TAG, "SyncLicensePlate(): Success!")
            changedDataCount
        } else {
            Log.d(TAG, "SyncLicensePlate(): Failed!", result.exceptionOrNull())
            0
        }
    }

    private suspend fun uploadAndRemoveAllTrackPoints(rootDoc: String): Int {
        val trackPointDAO = localDb.trackPointDAO
        val trackPoints = trackPointDAO.getAll()
        val trackPointCol = firestore.collection("$rootDoc/trackPoint")
        val result = firestore.runTransaction { transaction ->
            trackPoints.forEach { trackPoint ->
                val docRef = trackPointCol.document()
                transaction.set(docRef, trackPoint)
            }
        }.await()

        return if (result.isSuccess) {
            trackPointDAO.remove(*trackPoints.toTypedArray())
            Log.d(TAG, "UploadTrackPoint(): Success!")
            trackPoints.size
        } else {
            Log.d(TAG, "UploadTrackPoint(): Failed!", result.exceptionOrNull())
            0
        }
    }
}