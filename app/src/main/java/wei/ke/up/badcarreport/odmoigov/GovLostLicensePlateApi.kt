package wei.ke.up.badcarreport.odmoigov

import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.Request
import wei.ke.up.badcarreport.data.model.LicensePlate
import java.net.URI
import java.nio.charset.Charset

object GovLostLicensePlateApi {
    private val API_URL_PERFIX =
        "https://od.moi.gov.tw/adm/veh/query_veh?_m=query"
    private val okHttpClient = OkHttpClient()
    val TEST_LICENSE_NUMBER = "JFC-961"

    enum class VehicleType(val message: String = "") {
        /** Run with all matches */
        ALL,

        /** 汽車 */
        A("汽車"),

        /** 重機車 */
        B("重機車"),

        /** 輕機車 */
        C("輕機車"),

        /** 動力機械車 */
        G("動力機械"),
    }

    enum class VehicleStatus(val message: String) {
        /** 車輛失竊 */
        VEHICLE_LOST("車輛失竊"),

        /** 車牌失竊 */
        LICENSE_PLATE_LOST("車牌失竊"),

        /** 引擎失竊 */
        ENGINE_LOST("引擎失竊"),

        /** 查無資料 */
        DATA_NOT_FOUND("查無資料")
    }

    data class Result(
        val licenseNumber: String,
        val vehicleStatus: Map<VehicleType, VehicleStatus>,
    )

    suspend fun getLicensePlate(licenseNumber: String): LicensePlate {
        val result = getLicensePlateStatus(licenseNumber)
        val status =
            if (result.vehicleStatus.all { it.value == VehicleStatus.DATA_NOT_FOUND }) LicensePlate.Status.UNKNOWN
            else LicensePlate.Status.LOST
        return LicensePlate(licenseNumber, status)
    }

    suspend fun getLicensePlateStatus(
        licenseNumber: String,
        vehicleType: VehicleType = VehicleType.ALL
    ): Result {
        val requestUris = mutableListOf<URI>()

        when (vehicleType) {
            VehicleType.ALL -> {
                VehicleType.values()
                    .forEach {
                        if (it == VehicleType.ALL) return@forEach

                        requestUris += buildRequestUri(licenseNumber, it)
                    }
            }
            else -> {
                requestUris += buildRequestUri(licenseNumber, vehicleType)
            }
        }

        val vehicleStatus = mutableMapOf<VehicleType, VehicleStatus>()
        requestUris.forEach { uri ->
            val request = Request.Builder()
                .url(uri.toString())
                .build()
            coroutineScope {
                launch(Dispatchers.IO) {
                    okHttpClient.newCall(request)
                        .execute()
                        .use { response ->
                            val body = response.body!!
                            val result = decodeApiResponse(
                                body.byteString()
                                    .string(Charset.forName("Big5"))
                            )
                            vehicleStatus += result
                        }
                }
            }
        }

        return Result(licenseNumber, vehicleStatus)
    }

    private fun buildRequestUri(licenseNumber: String, vehicleType: VehicleType) =
        URI.create("$API_URL_PERFIX&vehType=${vehicleType.name}&vehNumber=$licenseNumber")

    private fun decodeApiResponse(body: String): Pair<VehicleType, VehicleStatus> {
        val data = body.lines()
            .last()
            .split(",")
        val vehicleType = VehicleType.values()
            .find { it.message == data[0] }!!
        val status = VehicleStatus.values()
            .find { it.message == data[2] }!!
        return vehicleType to status
    }
}