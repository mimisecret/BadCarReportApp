package wei.ke.up.badcarreport.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import wei.ke.up.badcarreport.data.repository.Repository
import wei.ke.up.badcarreport.databinding.FragmentSearchLicensePlateBinding

class SearchLicensePlateFragment : Fragment(), CoroutineScope by MainScope() {
    private var nullableBinding: FragmentSearchLicensePlateBinding? = null
    private val binding get() = nullableBinding!!
    private val repository by inject<Repository>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        nullableBinding =
            FragmentSearchLicensePlateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            searchText.apply {
                setOnEditorActionListener { _, actionId, _ ->
                    when (actionId) {
                        EditorInfo.IME_ACTION_SEARCH -> {
                            searchLicensePlate(editableText.toString())
                        }
                    }
                    true
                }
            }

            searchBtn.setOnClickListener {
                searchLicensePlate(searchText.text.toString())
            }
        }
    }

    private fun searchLicensePlate(licenseNumber: String) {
        showLoadingUI("搜尋中...")
        launch {
            val licensePlate = repository.getLicensePlate(licenseNumber)
            binding.apply {
                lpNumber.text = licensePlate.licenseNumber
                lpStatus.text = licensePlate.status.name
            }
            hideLoadingUI()
        }
    }

    private fun showLoadingUI(message: String) {
        binding.apply {
            lpNumber.text = ""
            lpStatus.text = message
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun hideLoadingUI() {
        binding.progressBar.visibility = View.INVISIBLE
    }

    override fun onDestroy() {
        cancel()
        nullableBinding = null
        super.onDestroy()
    }
}