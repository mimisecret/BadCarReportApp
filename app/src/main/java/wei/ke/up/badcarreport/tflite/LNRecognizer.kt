package wei.ke.up.badcarreport.tflite

import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.util.Log
import android.util.Size
import org.tensorflow.lite.Interpreter
import wei.ke.up.badcarreport.util.size
import wei.ke.up.badcarreport.util.toTensorImage
import java.io.FileInputStream
import java.nio.LongBuffer
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel

class LNRecognizer(context: Context) {
    companion object {
        private val TAG = this::class.java.simpleName
        private const val MODEL_FILE_NAME = "LicenseNumberModel.tflite"
        private const val CHARS = "ABCDEFGHJKLMNPQRSTUVWXYZ0123456789-"
    }

    private val interpreter: Interpreter
    private val outputData: LongBuffer

    val inputSize: Size

    init {
        try {
            val model = loadModel(context.assets)
            val option = Interpreter.Options().apply {
                setNumThreads(4)
            }
            interpreter = Interpreter(model, option)
            interpreter.allocateTensors()
            inputSize = getModelInputDataSize()
            outputData = LongBuffer.allocate(10)
        } catch (ex: Exception) {
            Log.e(TAG, "Load model error!", ex)
            throw ex
        }
    }

    private fun getModelInputDataSize() = interpreter.getInputTensor(0)
        .shape()
        .run {
            val height = this[1]
            val width = this[2]
            Size(width, height)
        }

    private fun loadModel(assetManager: AssetManager): MappedByteBuffer {
        val fd = assetManager.openFd(MODEL_FILE_NAME)
        val fis = FileInputStream(fd.fileDescriptor)
        val channel = fis.channel
        val startOffset = fd.startOffset
        val declaredLength = fd.declaredLength
        return channel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    fun recognize(bitmap: Bitmap): String {
        if (bitmap.size != inputSize) {
            throw IllegalArgumentException("Input bitmap size ${bitmap.size} != inputSize $inputSize")
        }

        val inputDataType = interpreter.getInputTensor(0)
            .dataType()
        val inputImage = bitmap.toTensorImage(inputDataType)

        outputData.setAllToAndRewind(-1)

        try {
            interpreter.run(inputImage.buffer, outputData)
        } catch (ex: Exception) {
            Log.e(TAG, "Recognizing error!", ex)
            throw RuntimeException(ex)
        }

        return outputData.mapToString(CHARS.toCharArray())
    }

    private fun LongBuffer.setAllToAndRewind(value: Long) = this.apply {
        for (i in 0 until capacity()) {
            put(i, value)
        }
        rewind()
    }

    private fun LongBuffer.mapToString(map: CharArray) = StringBuilder().let {
        for (i in 0 until capacity()) {
            if (this[i] != -1L) {
                it.append(map[this[i].toInt()])
            }
        }
        it.toString()
    }
}
