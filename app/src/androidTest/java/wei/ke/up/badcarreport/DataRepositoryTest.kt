package wei.ke.up.badcarreport

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.koin.test.inject
import wei.ke.up.badcarreport.data.model.LicensePlate
import wei.ke.up.badcarreport.data.model.TrackPoint
import wei.ke.up.badcarreport.data.repository.Repository
import wei.ke.up.badcarreport.util.await
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@RunWith(AndroidJUnit4::class)
class DataRepositoryTest : KoinTest {
    private val TAG = this::class.java.simpleName
    private val repository by inject<Repository>()
    private val remoteDocumentPath = "test/android"
    private lateinit var defaultData: List<Pair<LicensePlate, List<TrackPoint>>>

    private fun generateTestData(size: Int = 3): MutableList<Pair<LicensePlate, List<TrackPoint>>> {
        val result = mutableListOf<Pair<LicensePlate, List<TrackPoint>>>()

        for (i in 1..size) {
            val licenseNumber = generateLicenseNumber()
            val status = LicensePlate.Status.values()
            val trackPoints = mutableListOf<TrackPoint>().apply {
                repeat(2) {
                    add(generateTrackPoint(licenseNumber))
                }
            }
            val licensePlate = LicensePlate(
                licenseNumber,
                status[Random.nextInt(1, status.size)],
                "LP: $i"
            )
            result.add(licensePlate to trackPoints)
        }
        return result
    }

    private fun generateLicenseNumber(): String {
        val lNumber = StringBuilder()
        for (j in 1..Random.nextInt(2, 4)) {
            lNumber.append(Random.nextInt('A'.code, 'Z'.code + 1).toChar())
        }
        for (j in 1..4) {
            lNumber.append(Random.nextInt(0, 10).toString())
        }
        lNumber.insert(3, "-")
        return lNumber.toString()
    }

    private fun generateTrackPoint(licenseNumber: String): TrackPoint {
        return TrackPoint(
            licenseNumber = licenseNumber,
            latitude = Random.nextDouble(),
            longitude = Random.nextDouble()
        )
    }

    @Before
    fun before() = runBlocking {
        Log.d(TAG, "Run before block...")
        defaultData = generateTestData().apply {
            val lnFuan = "YB9-761"
            val lpFuan = LicensePlate(
                lnFuan,
                LicensePlate.Status.LOST,
                "此車輛在 110年 3月 3日，於高雄市建工路與大順路口遭竊。"
            )

            add(lpFuan to listOf(generateTrackPoint(lnFuan)))
        }

        defaultData.forEach { (licensePlate, trackPoints) ->
            trackPoints.forEach { trackPoint ->
                repository.addLicensePlate(licensePlate, trackPoint)
            }
        }
    }

    @After
    fun after() = runBlocking {
        Log.d(TAG, "Run after block...")
        repository.removeAllLicensePlate()
    }

    @Test
    fun localDBReadWriteTest() = runBlocking {
        val testData = generateTestData()
        testData.forEach { (licensePlate, trackPoints) ->
            trackPoints.forEach { trackPoint ->
                repository.addLicensePlate(licensePlate, trackPoint)
            }
        }
        testData.forEach { (licensePlate, trackPoints) ->
            val licensePlate = repository.getLicensePlate(licensePlate.licenseNumber)
            assertNotNull(licensePlate)
            assertEquals(licensePlate, licensePlate)
        }

        repository.removeLicensePlate(*testData.map { it.first }.toTypedArray())
    }

    @Test
    fun syncDbTest() = runBlocking {
        repository.syncDb(remoteDocumentPath)
        val trackPoints = repository.getAllTrackPoints()
        assertTrue(trackPoints.isEmpty())
        val localLps = repository.getAllLicensePlates()
        val remoteLps = getAllLicensePlateFromRemote()
        assertTrue(remoteLps.containsAll(localLps))
    }

    private suspend fun getAllLicensePlateFromRemote(): List<LicensePlate> {
        val rootDoc = "test/android"
        val firestore = Firebase.firestore
        val collection = firestore.collection("$rootDoc/licensePlate")
        return collection.get()
            .await()
            .getOrThrow()
            .documents
            .map { it.toObject<LicensePlate>()!! }
    }
}