package wei.ke.up.badcarreport

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.core.graphics.scale
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.qualifier.named
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.inject
import wei.ke.up.badcarreport.module.Recognizer
import wei.ke.up.badcarreport.test.R
import wei.ke.up.badcarreport.tflite.LNRecognizer
import wei.ke.up.badcarreport.tflite.LPRecognizer
import wei.ke.up.badcarreport.tflite.objr.ObjectRecognizer
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@RunWith(AndroidJUnit4::class)
class TFLiteTest : KoinTest {
    private val TAG = this::class.java.simpleName
    private lateinit var context: Context

    data class TestImage(
        val resourceId: Int,
        val licenseNumber: String,
        val bitmap: Bitmap
    )

    @Before
    fun before() {
        context = InstrumentationRegistry.getInstrumentation().context
    }

    @After
    fun after() {
    }

    @Test
    fun testLPRecognizer() {
        val testImage = BitmapFactory.decodeResource(context.resources, R.mipmap.car2)
        val lpRecognizer = get<LPRecognizer>()

        val lnResult = lpRecognizer.recognize(testImage)

        Log.d(TAG, lnResult.joinToString("\n"))
        assertTrue(lnResult.isNotEmpty())
    }

    @Test
    fun testObjectRecognizer() {
        val objRecognizer = get<ObjectRecognizer>(named(Recognizer.OBJECT))
        val testImage = BitmapFactory.decodeResource(context.resources, R.mipmap.car2)
        val scaledTestImage = Bitmap.createScaledBitmap(
            testImage,
            objRecognizer.inputSize.width,
            objRecognizer.inputSize.height,
            false
        )
        val result = objRecognizer.recognize(scaledTestImage)
        result.forEach {
            Log.d(TAG, it.toString())
            assertFalse(it.location.isEmpty)
        }
        assertTrue { result.isNotEmpty() }
    }

    @Test
    fun testLPObjectRecognizer() {
        val lpRecognizer = get<ObjectRecognizer>(named(Recognizer.LICENSE_PLATE))
        val testImage = BitmapFactory.decodeResource(context.resources, R.mipmap.one_car)
        val scaledTestImage = testImage.scale(
            lpRecognizer.inputSize.width,
            lpRecognizer.inputSize.height
        )
        val result = lpRecognizer.recognize(scaledTestImage)
            .filter { it.confidence >= 0.1 }
        Log.d(TAG, "License plate recognition: ${result.joinToString("\n")}")
        assertTrue { result.isNotEmpty() }
    }

    @Test
    fun testLNRecognizer() {
        val lnRecognizer by inject<LNRecognizer>()
        val testLicensePlates = mapOf(
            R.mipmap.mzk1586 to "MZK1586",
            R.mipmap.aw73rhw to "AW73RHW"
        ).map {
            val bitmap = BitmapFactory.decodeResource(context.resources, it.key)
            val scaledBitmap = Bitmap.createScaledBitmap(
                bitmap,
                lnRecognizer.inputSize.width,
                lnRecognizer.inputSize.height,
                false
            )
            TestImage(it.key, it.value, scaledBitmap)
        }

        testLicensePlates.forEach {
            val result = lnRecognizer.recognize(it.bitmap)
            if (result != it.licenseNumber) {
                Log.e(
                    TAG,
                    "Wrong Recognition! Expected: ${it.licenseNumber}, Actually: $result"
                )
            } else {
                Log.d(
                    TAG,
                    "Correct Recognition! Expected: ${it.licenseNumber}, Actually: $result"
                )
            }
        }
    }
}