package wei.ke.up.badcarreport

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import wei.ke.up.badcarreport.odmoigov.GovLostLicensePlateApi
import kotlin.test.assertEquals

class GovLostLicensePlateApiTest : KoinTest {
    private val govApi by inject<GovLostLicensePlateApi>()

    init {
        startKoin {
            printLogger()
            modules(
                module {
                    single { GovLostLicensePlateApi }
                }
            )
        }
    }

    @Test
    fun getLicensePlateStatus() = runBlocking {
        val testLicenseNumber = "JFC-961"
        val result = govApi.getLicensePlateStatus(testLicenseNumber)
        println(result)
        val expected = GovLostLicensePlateApi.Result(
            testLicenseNumber,
            mapOf(
                GovLostLicensePlateApi.VehicleType.A to GovLostLicensePlateApi.VehicleStatus.DATA_NOT_FOUND,
                GovLostLicensePlateApi.VehicleType.B to GovLostLicensePlateApi.VehicleStatus.VEHICLE_LOST,
                GovLostLicensePlateApi.VehicleType.C to GovLostLicensePlateApi.VehicleStatus.DATA_NOT_FOUND,
                GovLostLicensePlateApi.VehicleType.G to GovLostLicensePlateApi.VehicleStatus.DATA_NOT_FOUND,
            )
        )
        assertEquals(expected, result)
    }
}