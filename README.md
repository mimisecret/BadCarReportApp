# BadCarReport App

## Build Environments
- Minimum SDK version: 24
- Target SDK version: 30

## Build Apk
Run the following command at the project root folder to build debug apk.
```shell script
./gradlew assembleDebug
```
 After built, you can get the debug apk in `app/build/outputs/apk/debug/app-debug.apk`.